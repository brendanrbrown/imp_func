(* NOTE: I'm going to say doThingsAndStuff was intended to
   sort based on lexicographic order of the first letter
   of each word -- rather than the first character after each single space.
   So the funky version will consider consecutive spaces as
   a single gap between words, which `doThingsAndStuff` doesn't.*)

let doThingsFunky x =
  (* Remove strings with no spaces *)
  let filter_no_spaces = List.filter (fun s -> String.contains s ' ') in
  (* Descending lexicographic order on first char
     after a space. All else equal strings with more words
     are larger.*)
  let funky_lex_ord s1 s2 =
    let split_remove_empty s =
      String.split_on_char ' ' s |> List.filter (fun w -> String.length w > 0)
    in
    let ss1 = split_remove_empty s1 in
    let ss2 = split_remove_empty s2 in
    let compare_first w1 w2 = Char.compare w1.[0] w2.[0] in
    -List.compare compare_first ss1 ss2
  in
  filter_no_spaces x |> List.stable_sort funky_lex_ord

let () =
  let x = [ "aa b"; "az b"; "a c"; "a b c"; "dropit"; "a c u" ] in
  doThingsFunky x |> List.iter print_endline
