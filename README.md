Convert the impish script in `scripts/imp.js` to a funky functional style.

If you have `dune` and `opam`

```
./scripts/init-opam.sh && eval $(opam env)
dune exec imp_func
```
