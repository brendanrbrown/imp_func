#!/usr/bin/env bash

opam switch create . 5.1.1 --deps-only --with-test -y
# Enable the formatter
touch .ocamlformat
