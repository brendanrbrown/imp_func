// We're just tryina have a good time here.
// Sort sentences in descending lexicographic order of
// the first character after each single space, e.g. first character
// of each word, dropping single word sentences.
// x is mutated and holds the output after calling.
//
const doThingsAndStuff = x => {
  // Container for the result (in reverse order).
  const temp = []

  // Auxiliary variables.
  let temp2
  let tmep3
  let temp4

  while (x.length) {
    // NOTE: temp2 is a UTF-16 string, so
    // x is an array of strings.
    temp2 = x.pop()

    // Discard empty strings.
    if (!temp2) continue

    for (tmep3 = 0; tmep3 < temp2.length; tmep3++) {
      // Find the next space in temp2
      if (temp2 && temp2.charCodeAt(tmep3) == 32) {
        // Increment tmep3, returning previous value and adding one.
        // So start == tmep3.
        const start = tmep3++ + 1

        let doBreak = false
        let isFound = false

        // Loop over state of results.
        for (temp4 = 0; temp4 < temp.length; temp4++) {
          if (doBreak) {
            break
          }

          // Restart from first index after space in temp2.
          tmep3 = start

          isFound = false
          
          // Search over previously stored result at temp[temp4]
          for (let y = 0; y < temp[temp4].length; y++) {
            // Find the first space and then skip along to the next
            // character.
            if (!isFound && temp[temp4].charCodeAt(y) == 32) {
              isFound = true
              continue
            } else if (!isFound) {
              continue
            }

            // Check whether the first char after the first space in 
            // temp2 is smaller than the corresponding char in 
            // previous result at temp[temp4].
            // If so, insert temp2 at position temp4 and break the loop
            // over previous results, looping to find the next space
            // in temp2. Since temp2 is now a "previous result", it
            // will be inspected in subsequent temp4 loops.
            // If the two inspected chars are equal, skip to the next
            // space in temp2.
            if (!temp2.charCodeAt(tmep3) || temp2.charCodeAt(tmep3) <
temp[temp4].charCodeAt(y)) {
              // No-op
              temp4
              if (temp4 < 0) {
                temp4 = 0
              }
              // Insert temp2 at position temp4 if the first 
              // letter is smaller. Since this is on the _reversed_
              // results list, this gives _reverse_ lexicographical
              // order.
              temp.splice(temp4, 0, temp2)
              doBreak = true
              break
            } else if (temp2.charCodeAt(tmep3) == temp[temp4].charCodeAt(y)) {
              tmep3++
              continue
            } else {
              doBreak = true
              break
            }
          }
        }

        // Reset isFound after looping over all previous results.
        isFound = false

        // When temp2 is added to results in temp (in splice statement),
        // move on to the next element of x.
        for (temp4 = 0; temp4 < temp.length; temp4++) {
          if (temp2 === temp[temp4]) {
            isFound = true
            break
          }
        }
        // On first pass this is the final string in x,
        // (if it has at least one ' ')
        // Logic to see if we should add it
        if (!isFound) {
          temp.push(temp2)
        }

        // Move to the next element of x.
        break
      }
    }
  };

  // x = temp.reverse()
  while (temp.length) {
    const newThing = temp.pop()
    x.push(newThing)
  };
}
